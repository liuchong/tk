#!/bin/bash

SELFPATH=$(dirname $(readlink -f $0))

pids_path=${SELFPATH}/tmp/pids

for pid in $(ls ${pids_path}); do
  kill -INT $(cat ${pids_path}/${pid}) && \
    echo "$(basename $SELFPATH)(${pid}) stopped."
done
