class CreateChengsJzs < ActiveRecord::Migration
  def change
    create_table :chengs_jzs do |t|
      t.string :path
      t.string :title
      t.string :time
      t.string :user_name
      t.string :user_credit
      t.string :user_type
      t.string :user_date
      t.string :user_ip
      t.string :company
      t.string :info_id
      t.string :content
      t.string :mobile
      t.string :mobile_img

      t.timestamps
    end
  end
end
