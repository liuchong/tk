require 'test_helper'

class Chengs::JzsControllerTest < ActionController::TestCase
  setup do
    @chengs_jz = chengs_jzs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:chengs_jzs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create chengs_jz" do
    assert_difference('Chengs::Jz.count') do
      post :create, chengs_jz: @chengs_jz.attributes
    end

    assert_redirected_to chengs_jz_path(assigns(:chengs_jz))
  end

  test "should show chengs_jz" do
    get :show, id: @chengs_jz
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @chengs_jz
    assert_response :success
  end

  test "should update chengs_jz" do
    put :update, id: @chengs_jz, chengs_jz: @chengs_jz.attributes
    assert_redirected_to chengs_jz_path(assigns(:chengs_jz))
  end

  test "should destroy chengs_jz" do
    assert_difference('Chengs::Jz.count', -1) do
      delete :destroy, id: @chengs_jz
    end

    assert_redirected_to chengs_jzs_path
  end
end
