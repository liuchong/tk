#!/bin/bash

SELFPATH=$(dirname $(readlink -f $0))
PROJECT_NAME=$(basename $SELFPATH)
PORT=3004

port=${PORT}
env="development"

function start_server {
  if [ -e tmp/pids/server${port}.pid ]; then
    echo -n "${PROJECT_NAME}:${port} already running, "
    echo "PID: $(cat tmp/pids/server${port}.pid)"
    return
  fi 
  echo -n "${PROJECT_NAME}:${port} start"
  rails server -p $port -b 127.0.0.1 -e $env \
    -P ${SELFPATH}/tmp/pids/server${port}.pid -d \
    >> log/server${port}.out 2>> log/server_error${port}.out
  RESULT=$?
  echo -n ", "
  if [ "$RESULT" == "0" ]; then
    while true; do
      if [ -e tmp/pids/server${port}.pid ]; then
        echo "PID: $(cat tmp/pids/server${port}.pid)"
        break
      fi
      sleep 0.2
    done
  else
    echo "fail: $? !!!"
  fi
}

cd $SELFPATH

if [ "$1" == "production" ]; then
  env=$1
  echo environment=${env}
  for i in 0 1 2 3 4 5 6 7 8 9; do
    port=${PORT}$i
    start_server
  done
fi

port=${PORT}
start_server
