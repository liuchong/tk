# encoding: UTF-8
module Chengs
  class Jz < ActiveRecord::Base
    def self.fetch_1010jz
      Nokogiri::HTML(HTTPClient.get_content("http://sh.1010jz.com/")).css(
          "table.tablepix td a, table.tablepix_right td a").each do |link_a|
        link = link_a.attr("href")
        next if not link[/^http\:\/\//]
        clnt = HTTPClient.new
        doc = Nokogiri::HTML(clnt.get_content(link))
        l_info = doc.css("div.d_left").inner_html.encode("UTF-8")
        r_info = doc.css("div.d_right").inner_html.encode("UTF-8")
        jz = Jz.new
        jz.path = ""; doc.css("div.head>a").each { |l| jz.path << "/#{l.text}" }
        jz.title = doc.css("div.d_left>h2").first.text
        jz.time = l_info[/发布时间：([^<]*)/, 1]
        jz.user_name = r_info[/用户昵称：([^<]*)/, 1]
        jz.user_credit = r_info[/用户信用：([^<]*)/, 1]
        jz.user_type = r_info[/用户信用：([^<]*)/, 1]
        jz.user_date = r_info[/注册日期：([^<]*)/, 1]
        jz.user_ip = r_info[/发布人IP：([^<]*)/, 1]
        jz.company = l_info[/公司名称：([^<]*)/, 1]
        jz.info_id = l_info[/信息编号：([^\s]*)/, 1]
        next if Jz.where(info_id: jz.info_id).exists?
        jz.content = doc.css("div.d_content").inner_html.encode("UTF-8")
        img = doc.css("div.d_content>img").last
        img_path = "/assets/img/mobiles/#{jz.info_id}.jpg"
        File.open("#{XmhWeb::Application.config.paths["public"].first
            }#{img_path}", "wb") do |f|
          jz.mobile_img = img_path
          f.write clnt.get_content(URI::join(link, URI::encode(
            img.attr("src"))).to_s, nil, { Referer: link,
              "User-Agent" => "Opera/9.80 Presto/2.10.229 Version/11.61" })
        end if img
        jz.save
      end
    end
  end
end
