class ClientsController < ApplicationController
  def index
  end

  def show
    @client = { params[:id] => request.env[params[:id]] }
    @client = request.env if params[:id] == "all"
  end

end
