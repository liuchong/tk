# encoding: UTF-8
module Chengs
  class JzsController < ApplicationController
    # GET /chengs/jzs
    # GET /chengs/jzs.json
    def index
      @title = "兼职信息列表"
      @chengs_jzs = Jz.find(:all, order: "id DESC")

      respond_to do |format|
        format.html { render layout: "cheng" }
        format.json { render json: @chengs_jzs }
      end
    end

    def fetch_1010jz
      Jz.fetch_1010jz

      respond_to { |fmt| fmt.json { render json: { result: true } } }
    end

    # GET /chengs/jzs/1
    # GET /chengs/jzs/1.json
    def show
      @chengs_jz = Chengs::Jz.find(params[:id])

      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @chengs_jz }
      end
    end

    # GET /chengs/jzs/new
    # GET /chengs/jzs/new.json
    def new
      @chengs_jz = Chengs::Jz.new

      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @chengs_jz }
      end
    end

    # GET /chengs/jzs/1/edit
    def edit
      @chengs_jz = Chengs::Jz.find(params[:id])
    end

    # POST /chengs/jzs
    # POST /chengs/jzs.json
    def create
      @chengs_jz = Chengs::Jz.new(params[:chengs_jz])

      respond_to do |format|
        if @chengs_jz.save
          format.html { redirect_to @chengs_jz, notice: 'Jz was successfully created.' }
          format.json { render json: @chengs_jz, status: :created, location: @chengs_jz }
        else
          format.html { render action: "new" }
          format.json { render json: @chengs_jz.errors, status: :unprocessable_entity }
        end
      end
    end

    # PUT /chengs/jzs/1
    # PUT /chengs/jzs/1.json
    def update
      @chengs_jz = Chengs::Jz.find(params[:id])

      respond_to do |format|
        if @chengs_jz.update_attributes(params[:chengs_jz])
          format.html { redirect_to @chengs_jz, notice: 'Jz was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: "edit" }
          format.json { render json: @chengs_jz.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /chengs/jzs/1
    # DELETE /chengs/jzs/1.json
    def destroy
      @chengs_jz = Chengs::Jz.find(params[:id])
      @chengs_jz.destroy

      respond_to do |format|
        format.html { redirect_to chengs_jzs_url }
        format.json { head :no_content }
      end
    end
  end
end
